/****************************************************************************
** Meta object code from reading C++ file 'addmissionwidget.h'
**
** Created: Sat Jan 26 13:00:59 2013
**      by: The Qt Meta Object Compiler version 62 (Qt 4.6.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Space_War/addmissionwidget.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'addmissionwidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.6.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_AddMissionWidget[] = {

 // content:
       4,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      18,   17,   17,   17, 0x0a,
      39,   17,   17,   17, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_AddMissionWidget[] = {
    "AddMissionWidget\0\0recalculateSlot(int)\0"
    "startMissionSlot()\0"
};

const QMetaObject AddMissionWidget::staticMetaObject = {
    { &QGroupBox::staticMetaObject, qt_meta_stringdata_AddMissionWidget,
      qt_meta_data_AddMissionWidget, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &AddMissionWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *AddMissionWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *AddMissionWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_AddMissionWidget))
        return static_cast<void*>(const_cast< AddMissionWidget*>(this));
    return QGroupBox::qt_metacast(_clname);
}

int AddMissionWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QGroupBox::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: recalculateSlot((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: startMissionSlot(); break;
        default: ;
        }
        _id -= 2;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
