/****************************************************************************
** Meta object code from reading C++ file 'gamewidget.h'
**
** Created: Sat Jan 26 13:00:52 2013
**      by: The Qt Meta Object Compiler version 62 (Qt 4.6.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Space_War/gamewidget.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'gamewidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.6.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_GameWidget[] = {

 // content:
       4,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      18,   12,   11,   11, 0x0a,
      38,   12,   11,   11, 0x0a,
      61,   11,   11,   11, 0x0a,
      75,   11,   11,   11, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_GameWidget[] = {
    "GameWidget\0\0index\0tabChangedSlot(int)\0"
    "planetChangedSlot(int)\0refreshSlot()\0"
    "serverUpdateDataTimerSlot()\0"
};

const QMetaObject GameWidget::staticMetaObject = {
    { &QGroupBox::staticMetaObject, qt_meta_stringdata_GameWidget,
      qt_meta_data_GameWidget, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &GameWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *GameWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *GameWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_GameWidget))
        return static_cast<void*>(const_cast< GameWidget*>(this));
    return QGroupBox::qt_metacast(_clname);
}

int GameWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QGroupBox::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: tabChangedSlot((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: planetChangedSlot((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: refreshSlot(); break;
        case 3: serverUpdateDataTimerSlot(); break;
        default: ;
        }
        _id -= 4;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
