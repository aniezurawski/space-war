/****************************************************************************
** Meta object code from reading C++ file 'buildingwidget.h'
**
** Created: Sat Jan 26 13:00:53 2013
**      by: The Qt Meta Object Compiler version 62 (Qt 4.6.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Space_War/buildingwidget.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'buildingwidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.6.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_BuildingWidget[] = {

 // content:
       4,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      25,   16,   15,   15, 0x0a,
      80,   15,   15,   15, 0x0a,
      99,   15,   15,   15, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_BuildingWidget[] = {
    "BuildingWidget\0\0current,\0"
    "buildingChangedSlot(QListWidgetItem*,QListWidgetItem*)\0"
    "buildClickedSlot()\0activeClickedSlot()\0"
};

const QMetaObject BuildingWidget::staticMetaObject = {
    { &MainWidget::staticMetaObject, qt_meta_stringdata_BuildingWidget,
      qt_meta_data_BuildingWidget, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &BuildingWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *BuildingWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *BuildingWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_BuildingWidget))
        return static_cast<void*>(const_cast< BuildingWidget*>(this));
    return MainWidget::qt_metacast(_clname);
}

int BuildingWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = MainWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: buildingChangedSlot((*reinterpret_cast< QListWidgetItem*(*)>(_a[1])),(*reinterpret_cast< QListWidgetItem*(*)>(_a[2]))); break;
        case 1: buildClickedSlot(); break;
        case 2: activeClickedSlot(); break;
        default: ;
        }
        _id -= 3;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
