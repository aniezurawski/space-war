#ifndef DATABASE_H
#define DATABASE_H

#include <QtSql>
#include <QtSql>
#include <QVariant>
#include <QDebug>

class DataBase
{
    friend class MainWindow;

public:
    static bool userExists(const QString& login);
    static bool userExists(const QString& login, const QString& password);
    static QSqlRecord getUser(const QString& login);
    static void createUser(const QString& login, const QString& password);

    static QSqlQuery getGalaxyList();
    static QSqlRecord getGalaxy(const QString& name);

    static QSqlQuery getPlanetListByUser(const QString& userLogin);
    static QSqlQuery getPlanetListByGalaxy(const QString& galaxy);
    static QSqlRecord getPlanet(const QString& name);
    static int calculatePlanetEnergyProduction(const QString& name);
    static int calculatePlanetEnergyRequest(const QString& name);

    static QSqlQuery getBuildingTypeList();
    static QSqlRecord getBuilding(const QString& type, const QString& planet);
    static int buildingUnderConstructionCount();
    static void createBuilding(const QString& type, const QString& planet);
    static void upgradeBuilding(const QString& type, const QString& planet);
    static void setBuildingActive(const QString& type, const QString& planet, bool active);
    static bool shipyardWorks(const QString& planet);

    static QSqlQuery getShipTypeList(const QString& planet);
    static int getShipTypeCount();
    static void createShip(const QString& type, const QString& planet);

    static QSqlQuery getMissionList(const QString& planet);
    static QSqlQuery getMissionShipList(int id);
    static QSqlRecord getMission(int id);
    static QSqlRecord calculateMission(const QString& home, const QString& target);
    static void startMission(const QString& type, const QString& home, const QString& target, const std::vector<QString>& shipType, const std::vector<int>& shipCount, int cargo);

    static void serverUpdateData();

    ~DataBase();

private:
    DataBase();
    DataBase(const DataBase&);
    DataBase& operator =(const DataBase&);

    QSqlDatabase db;
};

#endif // DATABASE_H
