#include "mainwindow.h"
#include "loginwidget.h"
#include "gamewidget.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    LoginWidget* widget = new LoginWidget;
    setCentralWidget(widget);

    connect(widget, SIGNAL(loginSignal(QString)), this, SLOT(loginSlot(QString)));
}

MainWindow::~MainWindow()
{
    
}

void MainWindow::loginSlot(const QString &login)
{
    delete centralWidget();
    GameWidget* widget = new GameWidget(login);
    setCentralWidget(widget);
}
