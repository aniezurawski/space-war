#include "missionswidget.h"
#include "database.h"
#include <QLayout>

MissionsWidget::MissionsWidget(const PlanetRecord &planet, QWidget *parent) :
    MainWidget("Missions", planet, parent),
    missionList(new QListWidget),
    missionInfo(new QGroupBox("Details")),
    fleetInfo(new QGroupBox("Fleet")),
    typeLabel(new QLabel),
    targetLabel(new QLabel),
    progressLabel(new QLabel),
    cargoLabel(new QLabel),
    shipLabel(mission.shipName.size())
{
    QBoxLayout* typeLay = new QHBoxLayout;
    typeLay->addWidget(new QLabel("Type: "));
    typeLay->addWidget(typeLabel);

    QBoxLayout* targetLay = new QHBoxLayout;
    targetLay->addWidget(new QLabel("Target: "));
    targetLay->addWidget(targetLabel);

    QBoxLayout* progressLay = new QHBoxLayout;
    progressLay->addWidget(new QLabel("Progress: "));
    progressLay->addWidget(progressLabel);

    QBoxLayout* cargoLay = new QHBoxLayout;
    cargoLay->addWidget(new QLabel("Cargo: "));
    cargoLay->addWidget(cargoLabel);

    QBoxLayout* infoLay = new QVBoxLayout;
    infoLay->addLayout(typeLay);
    infoLay->addLayout(targetLay);
    infoLay->addLayout(progressLay);
    infoLay->addLayout(cargoLay);
    missionInfo->setLayout(infoLay);

    QBoxLayout* fleetLay = new QVBoxLayout;
    for (unsigned int i = 0; i < shipLabel.size(); i++) {
        shipLabel[i] = new QLabel();
        fleetLay->addWidget(shipLabel[i]);
    }
    fleetInfo->setLayout(fleetLay);

    QGridLayout* layout = new QGridLayout;
    layout->addWidget(missionList, 0, 0, Qt::AlignLeft);
    layout->addWidget(missionInfo, 0, 1, Qt::AlignLeft);
    layout->addWidget(fleetInfo, 0, 2, Qt::AlignLeft);
    setLayout(layout);

    planetUpdate(planet);
}

void MissionsWidget::planetUpdate(const PlanetRecord &planet)
{
    disconnect(missionList, SIGNAL(currentRowChanged(int)), this, SLOT(missionChangedSlot(int)));

    missionMap.clear();
    missionList->clear();
    QSqlQuery query = DataBase::getMissionList(planet.name);
    for (int i = 0; query.next(); i++) {
        int id = query.record().value("id").toInt();
        QString type = query.record().value("type").toString();
        missionMap[i] = id;
        missionList->addItem(type);
    }

    missionList->setCurrentRow(0);
    connect(missionList, SIGNAL(currentRowChanged(int)), this, SLOT(missionChangedSlot(int)));
    missionChangedSlot(0);
}

void MissionsWidget::missionChangedSlot(int row)
{
    mission = DataBase::getMission(missionMap[row]);
    typeLabel->setText(mission.type);
    targetLabel->setText(mission.target);
    progressLabel->setText(QString::number(mission.progress) + " / " + QString::number(mission.time) + (mission.ret ? " (return)" : ""));
    cargoLabel->setText(QString::number(mission.cargo));

    for (unsigned int i = 0; i < shipLabel.size(); i++) {
        shipLabel[i]->setText(mission.shipName[i] + ": " + QString::number(mission.shipCount[i]));
    }
}
