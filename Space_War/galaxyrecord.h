#ifndef GALAXYRECORD_H
#define GALAXYRECORD_H

#include "point3.h"
#include <QString>
#include <QtSql>

class GalaxyRecord
{
public:
    GalaxyRecord() {}
    GalaxyRecord(const QSqlRecord& record);

    QString name;
    Point3i coord;
};

#endif // GALAXYRECORD_H
