#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include "planetrecord.h"
#include <QString>
#include <QGroupBox>

class MainWidget : public QGroupBox
{
    Q_OBJECT
public:
    explicit MainWidget(const QString& name, const PlanetRecord& planet, QWidget *parent = 0);

    void setPlanet(const PlanetRecord& planet) { this->planet = planet; planetUpdate(planet); }
    const PlanetRecord& getPlanet() const { return planet; }

    virtual void planetUpdate(const PlanetRecord& planet) = 0;
    
signals:
    void refresh();
    
public slots:

private:
    PlanetRecord planet;
    
};

#endif // MAINWIDGET_H
