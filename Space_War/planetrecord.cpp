#include "planetrecord.h"
#include "database.h"

PlanetRecord::PlanetRecord(const QSqlRecord &record) :
    name(record.value("name").toString()),
    coord(record.value("coord_x").toInt(), record.value("coord_y").toInt(), record.value("coord_z").toInt()),
    energyPotential(record.value("energy_potential").toInt()),
    metal(record.value("metal").toInt()),
    deuter(record.value("deuter").toInt()),
    user(record.value("user").toString()),
    galaxy(record.value("galaxy").toString()),
    energyProduction(0),
    energyRequest(0)
{
    recalculateEnergy();
}

void PlanetRecord::recalculateEnergy()
{
    energyProduction = DataBase::calculatePlanetEnergyProduction(name);
    energyRequest = DataBase::calculatePlanetEnergyRequest(name);
}
