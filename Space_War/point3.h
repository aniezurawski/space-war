#ifndef POINT_H
#define POINT_H

template <typename T>
class Point3 {
public:
    Point3() : x(0), y(0), z(0) {}
    Point3(const T& x, const T&y, const T& z) : x(x), y(y), z(z) {}
    T x;
    T y;
    T z;
};

typedef Point3<int> Point3i;

#endif // POINT_H
