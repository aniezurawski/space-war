#include "missionrecord.h"
#include "database.h"

MissionRecord::MissionRecord() :
    shipName(DataBase::getShipTypeCount()),
    shipCount(shipName.size())
{
}

MissionRecord::MissionRecord(const QSqlRecord& record) :
    id(record.value("id").toInt()),
    type(record.value("type").toString()),
    target(record.value("target").toString()),
    time(record.value("time").toInt()),
    progress(record.value("progress").toInt()),
    ret(record.value("return").toBool()),
    cargo(record.value("cargo").toInt()),
    shipName(DataBase::getShipTypeCount()),
    shipCount(shipName.size())
{
    recalculateShips();
}

void MissionRecord::recalculateShips()
{
    QSqlQuery query = DataBase::getMissionShipList(id);
    for (int i = 0; query.next(); ++i) {
        shipName[i] = query.record().value("type").toString();
        shipCount[i] = query.record().value("count").toInt();
    }
}
