#ifndef BUILDINGRECORD_H
#define BUILDINGRECORD_H

#include <QString>
#include <QtSql>

class BuildingRecord
{
public:
    BuildingRecord() {}
    BuildingRecord(const QSqlRecord& record);

    QString type;
    bool exists;
    QString planet;
    int level;
    bool isActive;
    int constructionProgress;
    int constructionEnergyRequest;
    int workingEnergyRequest;
    int constructionTime;
    int metalCost;

};

#endif // BUILDINGRECORD_H
