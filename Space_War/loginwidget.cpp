#include "loginwidget.h"
#include "database.h"
#include <QLayout>
#include <QPushButton>
#include <QtSql/QSqlQuery>
#include <QVariant>
#include <QDebug>
#include <QCryptographicHash>

LoginWidget::LoginWidget(QWidget *parent) :
    QWidget(parent),
    newUserCheckBox(new QCheckBox),
    loginBox(new QLineEdit),
    passwordBox(new QLineEdit),
    repeatPassBox(new QLineEdit),
    button(new QPushButton),
    errorLabel(new QLabel)
{
    QLabel* newUserLabel = new QLabel("New user: ");
    QLabel* loginLabel = new QLabel("User: ");
    QLabel* passwordLabel = new QLabel("Password: ");
    QLabel* repeatPassLabel = new QLabel("Repeat password.: ");
    QLayout* labelLayout = new QVBoxLayout();
    labelLayout->addWidget(newUserLabel);
    labelLayout->addWidget(loginLabel);
    labelLayout->addWidget(passwordLabel);
    labelLayout->addWidget(repeatPassLabel);

    QLayout* boxLayout = new QVBoxLayout();
    boxLayout->addWidget(newUserCheckBox);
    boxLayout->addWidget(loginBox);
    boxLayout->addWidget(passwordBox);
    boxLayout->addWidget(repeatPassBox);

    QBoxLayout* formLayout = new QHBoxLayout();
    formLayout->addLayout(labelLayout);
    formLayout->addLayout(boxLayout);

    QBoxLayout* layout = new QVBoxLayout();
    layout->addLayout(formLayout);
    layout->addWidget(button);
    layout->addWidget(errorLabel);

    setLayout(layout);
    newUserCheckBox->setCheckState(Qt::Unchecked);
    newUserCheckBoxChangeStateSlot(Qt::Unchecked);
    passwordBox->setEchoMode(QLineEdit::Password);
    repeatPassBox->setEchoMode(QLineEdit::Password);
    errorLabel->setWordWrap(true);
    errorLabel->setFixedHeight(50);

    connect(button, SIGNAL(clicked()), this, SLOT(buttonSlot()));
    connect(newUserCheckBox, SIGNAL(stateChanged(int)), this, SLOT(newUserCheckBoxChangeStateSlot(int)));
}

void LoginWidget::login()
{
    if (!isLoginCorrect()) {
        errorLabel->setText(QString::fromUtf8("Niedozwolony login."));
        return;
    }

    if (!DataBase::userExists(loginBox->text(), passwordBox->text())) {
        errorLabel->setText(QString::fromUtf8("Niepoprawny login lub hasło."));
        return;
    }

    emit loginSignal(loginBox->text());
}

void LoginWidget::registration()
{
    if (!isLoginCorrect()) {
        errorLabel->setText(QString::fromUtf8("Niedozwolony login."));
        return;
    }

    if (DataBase::userExists(loginBox->text())) {
        errorLabel->setText(QString::fromUtf8("Użytkownik o podanej nazwie już istnieje."));
        return;
    }

    if (passwordBox->text() != repeatPassBox->text()) {
        errorLabel->setText(QString::fromUtf8("Hasła rónią się."));
        return;
    }

    DataBase::createUser(loginBox->text(), passwordBox->text());

    emit loginSignal(loginBox->text());
}

bool LoginWidget::isLoginCorrect()
{
    QString login = loginBox->text();

    if (login.isEmpty() || login.length() > 50) {
        return false;
    }

    for (QString::Iterator it = login.begin(); it != login.end(); it++) {
        if (!(it->isLetterOrNumber() || *it == '_')) {
            return false;
        }
    }
    return true;
}

void LoginWidget::buttonSlot()
{
    if (newUserCheckBox->checkState() == Qt::Unchecked) {
        login();
    } else {
        registration();
    }
}

void LoginWidget::newUserCheckBoxChangeStateSlot(int state)
{
    if (state == Qt::Checked) {
        repeatPassBox->setEnabled(true);
        button->setText("Register");
    } else {
        repeatPassBox->setEnabled(false);
        button->setText("Login");
    }
}
