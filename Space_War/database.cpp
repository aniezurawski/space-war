#include "database.h"

DataBase::DataBase() :
    db(QSqlDatabase::addDatabase("QPSQL"))
{
    /*db.setDatabaseName("bd");
    db.setHostName("labdb");
    db.setUserName("an321133");
    db.setPassword("an321133");*/
    db.setHostName("localhost");
    db.setUserName("postgres");
    db.setPassword("umyjbrode666");

    if (!db.open()) {
        qDebug() << "Błąd: nie można się połączyć z bazą!";
        throw 0;
    }
}

DataBase::~DataBase()
{
    db.close();
}

bool DataBase::userExists(const QString &login)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM \"user\" WHERE login = :login ;");
    qDebug() << query.lastError();
    query.bindValue(":login", login);
    query.exec();
    qDebug() << query.lastError();

    return query.next();
}

bool DataBase::userExists(const QString &login, const QString& password)
{
    QString passwordHash = QCryptographicHash::hash(QByteArray(password.toStdString().data()), QCryptographicHash::Md5);
    QSqlQuery query;
    query.prepare("SELECT * FROM \"user\" WHERE login = :login AND password = :password ;");
    qDebug() << query.lastError();
    query.bindValue(":login", login);
    query.bindValue(":password", passwordHash);
    query.exec();
    qDebug() << query.lastError();

    return query.next();
}

QSqlRecord DataBase::getUser(const QString &login)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM \"user\" WHERE login = :login ;");
    qDebug() << query.lastError();
    query.bindValue(":login", login);
    query.exec();
    qDebug() << query.lastError();

    query.next();
    return query.record();
}

void DataBase::createUser(const QString &login, const QString &password)
{
    QString passwordHash = QCryptographicHash::hash(QByteArray(password.toStdString().data()), QCryptographicHash::Md5);
    QSqlQuery query;
    query.prepare("INSERT INTO \"user\" (login, password) VALUES (:login, :password);");
    qDebug() << query.lastError();
    query.bindValue(":login", login);
    query.bindValue(":password", passwordHash);
    query.exec();
    qDebug() << query.lastError();
}

QSqlRecord DataBase::getGalaxy(const QString &name)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM galaxy WHERE name = :name ;");
    qDebug() << query.lastError();
    query.bindValue(":name", name);
    query.exec();
    qDebug() << query.lastError();

    query.next();
    return query.record();
}

QSqlQuery DataBase::getGalaxyList()
{
    QSqlQuery query;
    query.prepare(QString("SELECT name FROM galaxy;"));
    qDebug() << query.lastError();
    query.exec();
    qDebug() << query.lastError();

    return query;
}

QSqlQuery DataBase::getPlanetListByUser(const QString &userLogin)
{
    QSqlQuery query;
    query.prepare("SELECT name FROM planet WHERE \"user\" = :login ;");
    qDebug() << query.lastError();
    query.bindValue(":login", userLogin);
    query.exec();
    qDebug() << query.lastError();

    return query;
}

QSqlQuery DataBase::getPlanetListByGalaxy(const QString &galaxy)
{
    QSqlQuery query;
    query.prepare("SELECT name FROM planet WHERE galaxy = :galaxy ;");
    qDebug() << query.lastError();
    query.bindValue(":galaxy", galaxy);
    query.exec();
    qDebug() << query.lastError();

    return query;
}

QSqlRecord DataBase::getPlanet(const QString &name)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM planet WHERE name = :name ;");
    qDebug() << query.lastError();
    query.bindValue(":name", name);
    query.exec();
    qDebug() << query.lastError();

    query.next();
    return query.record();
}

int DataBase::calculatePlanetEnergyProduction(const QString &name)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM planet_energy_production WHERE planet = :name ;");
    qDebug() << query.lastError();
    query.bindValue(":name", name);
    query.exec();
    qDebug() << query.lastError();

    if (!query.next())
        return 0;
    return query.record().value("energy_production").toInt();
}

int DataBase::calculatePlanetEnergyRequest(const QString &name) {
    QSqlQuery query;
    query.prepare("SELECT * FROM planet_energy_request WHERE planet = :name ;");
    qDebug() << query.lastError();
    query.bindValue(":name", name);
    query.exec();
    qDebug() << query.lastError();

    if (!query.next())
        return 0;
    return query.record().value("energy_request").toInt();
}

QSqlQuery DataBase::getBuildingTypeList()
{
    QSqlQuery query;
    query.prepare("SELECT type FROM building_type ORDER BY metal_cost;");
    qDebug() << query.lastError();
    query.exec();
    qDebug() << query.lastError();

    return query;
}

QSqlRecord DataBase::getBuilding(const QString &type, const QString &planet)
{
    QSqlQuery query;
    query.prepare("SELECT *, t.type AS real_type "
                  "FROM (SELECT * FROM building WHERE type = :type1 AND planet = :planet ) b "
                  "RIGHT JOIN (SELECT * FROM building_type WHERE type = :type2 ) t ON b.type = t.type;");
    qDebug() << query.lastError();
    query.bindValue(":type1", type);
    query.bindValue(":type2", type);
    query.bindValue(":planet", planet);
    query.exec();
    qDebug() << query.lastError();

    query.next();
    return query.record();
}

int DataBase::buildingUnderConstructionCount()
{
    QSqlQuery query;
    query.prepare("SELECT count(*) AS count FROM building_under_construction;");
    qDebug() << query.lastError();
    query.exec();
    qDebug() << query.lastError();

    query.next();
    return query.record().value("count").toInt();
}

void DataBase::createBuilding(const QString &type, const QString &planet)
{
    QSqlQuery query;
    query.prepare("INSERT INTO building (type, planet, level, is_active, construction_progress) "
                  "VALUES (:type1, :planet, 1, TRUE, (SELECT construction_time FROM building_type WHERE type = :type2));");
    qDebug() << query.lastError();
    query.bindValue(":type1", type);
    query.bindValue(":type2", type);
    query.bindValue(":planet", planet);
    query.exec();
    qDebug() << query.lastError();
}

void DataBase::upgradeBuilding(const QString &type, const QString &planet)
{
    QSqlQuery query;
    query.prepare("UPDATE building SET level = level + 1, construction_progress = (SELECT construction_time FROM building_type WHERE type = :type1) WHERE type = :type2 AND planet = :planet ;");
    qDebug() << query.lastError();
    query.bindValue(":type1", type);
    query.bindValue(":type2", type);
    query.bindValue(":planet", planet);
    query.exec();
    qDebug() << query.lastError();
}

void DataBase::setBuildingActive(const QString &type, const QString &planet, bool active)
{
    QString activeStr = (active ? "TRUE" : "FALSE");
    QSqlQuery query;
    query.prepare("UPDATE building SET is_active = :active WHERE type = :type AND planet = :planet;");
    qDebug() << query.lastError();
    query.bindValue(":type", type);
    query.bindValue(":planet", planet);
    query.bindValue(":active", activeStr);
    query.exec();
    qDebug() << query.lastError();
}

bool DataBase::shipyardWorks(const QString &planet)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM shipyard_ok WHERE planet = :planet ;");
    qDebug() << query.lastError();
    query.bindValue(":planet", planet);
    query.exec();
    qDebug() << query.lastError();

    query.next();
    return query.record().value("ok").toBool();
}

QSqlQuery DataBase::getShipTypeList(const QString &planet)
{
    QSqlQuery query;
    query.prepare("WITH construction AS (SELECT * FROM ship_stationed WHERE construction_progress > 0 AND home_planet = :planet1)"
                  "SELECT *, "
                  "(SELECT count(*) FROM ship_stationed  WHERE type = t.type AND construction_progress = 0 AND home_planet = :planet2) AS stationed, "
                  "(SELECT count(*) FROM ship_on_mission WHERE type = t.type AND home_planet = :planet3) AS on_mission, "
                  "(SELECT count(*) FROM construction WHERE type = t.type) AS during_construction, "
                  "(SELECT min(construction_progress) AS construction_progress FROM construction WHERE type = t.type) AS construction_progress "
                  "FROM ship_type t ORDER BY metal_cost;");
    qDebug() << query.lastError();
    query.bindValue(":planet1", planet);
    query.bindValue(":planet2", planet);
    query.bindValue(":planet3", planet);
    query.exec();
    qDebug() << query.lastError();

    return query;
}

int DataBase::getShipTypeCount()
{
    QSqlQuery query;
    query.prepare("SELECT count(*) FROM ship_type;");
    qDebug() << query.lastError();
    query.exec();
    qDebug() << query.lastError();
    query.next();
    return query.record().value("count").toInt();
}

void DataBase::createShip(const QString &type, const QString &planet)
{
    QSqlQuery query;
    query.prepare("INSERT INTO ship (type, home_planet, mission, construction_progress) VALUES (:type1, :planet, NULL, (SELECT construction_time FROM ship_type WHERE type = :type2));");
    qDebug() << query.lastError();
    query.bindValue(":type1", type);
    query.bindValue(":type2", type);
    query.bindValue(":planet", planet);
    query.exec();
    qDebug() << query.lastError();
}

QSqlQuery DataBase::getMissionList(const QString &planet)
{
    QSqlQuery query;
    query.prepare("SELECT m.id, m.type FROM (SELECT mission FROM ship WHERE mission IS NOT NULL AND home_planet = :planet) s LEFT JOIN mission m ON s.mission = m.id GROUP BY m.id, m.type;");
    qDebug() << query.lastError();
    query.bindValue(":planet", planet);
    query.exec();
    qDebug() << query.lastError();

    return query;
}

QSqlQuery DataBase::getMissionShipList(int id)
{
    QSqlQuery query;
    query.prepare("SELECT st.type, count(s.type) FROM ship_type st LEFT JOIN (SELECT type FROM ship WHERE mission = :id) s ON st.type = s.type GROUP BY st.type;");
    qDebug() << query.lastError();
    query.bindValue(":id", id);
    query.exec();
    qDebug() << query.lastError();

    return query;
}

QSqlRecord DataBase::getMission(int id)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM mission WHERE id = :id;");
    qDebug() << query.lastError();
    query.bindValue(":id", id);
    query.exec();
    qDebug() << query.lastError();
    query.next();
    qDebug() << query.lastError();

    return query.record();
}

void DataBase::serverUpdateData()
{
    QSqlQuery query;
    query.prepare("SELECT update_data();");
    //qDebug() << query.lastError();
    query.exec();
    //qDebug() << query.lastError();
}

QSqlRecord DataBase::calculateMission(const QString &home, const QString &target)
{
    QSqlQuery query;
    query.prepare("WITH time_deuter AS (SELECT * FROM calculate_mission(:home1, :target1)) "
                  "SELECT :home2::text AS home_planet, "
                  "(SELECT galaxy FROM planet WHERE name = :home3) AS home_galaxy, "
                  ":target2::text AS target_planet, "
                  "(SELECT galaxy FROM planet WHERE name = :target3) AS target_galaxy, "
                  "(SELECT time FROM time_deuter) AS time, "
                  "(SELECT deuter FROM time_deuter) AS deuter;");
    qDebug() << query.lastError();
    query.bindValue(":home1", home);
    query.bindValue(":target1", target);
    query.bindValue(":home2", home);
    query.bindValue(":target2", target);
    query.bindValue(":home3", home);
    query.bindValue(":target3", target);
    query.exec();
    qDebug() << query.lastError();

    query.next();
    return query.record();
}

void DataBase::startMission(const QString &type, const QString& home, const QString &target, const std::vector<QString> &shipType, const std::vector<int> &shipCount, int cargo)
{
    int totalShipCount = 0;
    for (unsigned int i = 0; i < shipCount.size(); i++) {
        totalShipCount += shipCount[i];
    }

    int id = 0;
    QSqlQuery query;
    query.prepare("SELECT max(id)+1 AS id FROM mission;");
    qDebug() << query.lastError();
    query.exec();
    qDebug() << query.lastError();

    if (query.next())
        id = query.record().value("id").toInt();

    query.prepare("INSERT INTO mission (id, type, target, time, progress, cargo, return) "
                  "VALUES(:id, :type, :target1, (SELECT time FROM calculate_mission(:home, :target2)), 0, :cargo, FALSE);");
    qDebug() << query.lastError();
    query.bindValue(":type", type);
    query.bindValue(":home", home);
    query.bindValue(":target1", target);
    query.bindValue(":target2", target);
    query.bindValue(":id", id);
    query.bindValue(":cargo", cargo);
    query.exec();
    qDebug() << query.lastError();

    query.prepare(QString("UPDATE planet SET ")
               + "deuter = deuter - (SELECT deuter FROM calculate_mission(:home1, :target)) * :totalship, "
               + "metal = metal - :cargo "
               + "WHERE name = :home2;");
    qDebug() << query.lastError();
    query.bindValue(":type", type);
    query.bindValue(":home1", home);
    query.bindValue(":home2", home);
    query.bindValue(":target", target);
    query.bindValue(":totalship", totalShipCount);
    query.bindValue(":cargo", cargo);
    query.exec();
    qDebug() << query.lastError();

    for (unsigned int i = 0; i < shipCount.size(); i++) {
        query.prepare("UPDATE ship SET mission = :id "
                      "WHERE id IN "
                      "(SELECT id FROM ship_stationed WHERE home_planet = :home AND type = :type AND construction_progress = 0 LIMIT :count);");
        qDebug() << query.lastError();
        query.bindValue(":type", shipType[i]);
        query.bindValue(":home", home);
        query.bindValue(":count", QString::number(shipCount[i]));
        query.bindValue(":id", id);
        query.exec();
        qDebug() << query.lastError();
    }
}
