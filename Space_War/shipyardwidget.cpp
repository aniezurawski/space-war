#include "shipyardwidget.h"
#include "database.h"
#include <QLayout>
#include <QLabel>
#include <QButtonGroup>

ShipyardWidget::ShipyardWidget(const PlanetRecord& planet, QWidget *parent) :
    MainWidget("Shipyard", planet, parent)
{
    int shipTypeCount = DataBase::getShipTypeCount();
    QGridLayout* layout = new QGridLayout;
    QButtonGroup* buildButtonGroup = new QButtonGroup;
    connect(buildButtonGroup, SIGNAL(buttonClicked(int)), this, SLOT(buildClickedSlot(int)));

    shipNameLabel.resize(shipTypeCount);
    shipStationedLabel.resize(shipTypeCount);
    shipMissionLabel.resize(shipTypeCount);
    shipArmourLabel.resize(shipTypeCount);
    shipAttackLabel.resize(shipTypeCount);
    shipCapacityLabel.resize(shipTypeCount);
    shipCostLabel.resize(shipTypeCount);
    shipConstructionLabel.resize(shipTypeCount);
    shipAddButton.resize(shipTypeCount);

    layout->addWidget(new QLabel("Name  "), 0, 0);
    layout->addWidget(new QLabel("Stationed  "), 0, 1);
    layout->addWidget(new QLabel("On mission  "), 0, 2);
    layout->addWidget(new QLabel("Armour  "), 0, 3);
    layout->addWidget(new QLabel("Atack  "), 0, 4);
    layout->addWidget(new QLabel("Capacity  "), 0, 5);
    layout->addWidget(new QLabel("Cost  "), 0, 6);
    layout->addWidget(new QLabel("Construction "), 0, 7);

    for (int i = 0; i < shipTypeCount; i++) {
        shipNameLabel[i] = new QLabel;
        shipStationedLabel[i] = new QLabel;
        shipMissionLabel[i] = new QLabel;
        shipArmourLabel[i] = new QLabel;
        shipAttackLabel[i] = new QLabel;
        shipCapacityLabel[i] = new QLabel;
        shipCostLabel[i] = new QLabel;
        shipConstructionLabel[i] = new QLabel;

        layout->addWidget(shipNameLabel[i], i + 1, 0);
        layout->addWidget(shipStationedLabel[i], i + 1, 1);
        layout->addWidget(shipMissionLabel[i], i + 1, 2);
        layout->addWidget(shipArmourLabel[i], i + 1, 3);
        layout->addWidget(shipAttackLabel[i], i + 1, 4);
        layout->addWidget(shipCapacityLabel[i], i + 1, 5);
        layout->addWidget(shipCostLabel[i], i + 1, 6);

        QBoxLayout* conItemLay = new QHBoxLayout;
        conItemLay->addWidget(shipConstructionLabel[i]);
        shipAddButton[i] = new QPushButton("Build");
        conItemLay->addWidget(shipAddButton[i]);
        layout->addLayout(conItemLay, i + 1, 7);
        buildButtonGroup->addButton(shipAddButton[i], i);
    }

    setLayout(layout);

    planetUpdate(planet);
}

void ShipyardWidget::planetUpdate(const PlanetRecord &planet)
{
    if (DataBase::shipyardWorks(planet.name)) {
        setEnabled(true);
    } else {
        setEnabled(false);
    }

    QSqlQuery query = DataBase::getShipTypeList(planet.name);
    for (int i = 0; query.next(); ++i) {
        shipNameLabel[i]->setText( query.record().value("type").toString() );
        shipStationedLabel[i]->setText( query.record().value("stationed").toString() );
        shipMissionLabel[i]->setText( query.record().value("on_mission").toString() );
        shipArmourLabel[i]->setText( query.record().value("armour").toString() );
        shipAttackLabel[i]->setText( query.record().value("attack").toString() );
        shipCapacityLabel[i]->setText( query.record().value("capacity").toString() );
        shipCostLabel[i]->setText( query.record().value("metal_cost").toString() );
        shipConstructionLabel[i]->setText( query.record().value("during_construction").toString()
                + " (" + QString::number(query.record().value("construction_time").toInt() - query.record().value("construction_progress").toInt()) + "/"
                + query.record().value("construction_time").toString() + ")" );

        if (planet.metal >= query.record().value("metal_cost").toInt()) {
            shipAddButton[i]->setEnabled(true);
        } else {
            shipAddButton[i]->setEnabled(false);
        }
    }
}

void ShipyardWidget::buildClickedSlot(int id)
{
    DataBase::createShip(shipNameLabel[id]->text(), getPlanet().name);
    emit refresh();
}
