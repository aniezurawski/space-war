#ifndef MISSIONSWIDGET_H
#define MISSIONSWIDGET_H

#include "mainwidget.h"
#include "missionrecord.h"
#include <QLabel>
#include <QPushButton>
#include <QListWidget>
#include <vector>
#include <QGroupBox>

class MissionsWidget : public MainWidget
{
    Q_OBJECT
public:
    explicit MissionsWidget(const PlanetRecord& planet, QWidget *parent = 0);
    virtual void planetUpdate(const PlanetRecord &planet);
    
signals:
    
public slots:
    void missionChangedSlot(int row);

private:
    MissionRecord mission;
    std::map<int, int> missionMap;

    QListWidget* missionList;
    QGroupBox* missionInfo;
    QGroupBox* fleetInfo;

    QLabel* typeLabel;
    QLabel* targetLabel;
    QLabel* progressLabel;
    QLabel* cargoLabel;

    std::vector<QLabel*> shipLabel;

};

#endif // MISSIONSWIDGET_H
