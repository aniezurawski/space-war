#include "universewidget.h"
#include "database.h"
#include "galaxyrecord.h"
#include "addmissionwidget.h"

UniverseWidget::UniverseWidget(const PlanetRecord &planet, QWidget *parent) :
    MainWidget("Universe", planet, parent),
    galaxyBox(new QComboBox),
    galaxyCoord(new QLabel),
    planetBox(new QComboBox),
    planetCoord(new QLabel),
    energyPotential(new QLabel),
    user(new QLabel),
    colonizationButton(new QPushButton("Colonization")),
    pillageButton(new QPushButton("Pillage")),
    transportButton(new QPushButton("Transport"))
{
    QBoxLayout* hBoxGalaxy = new QHBoxLayout;
    hBoxGalaxy->addWidget(galaxyBox);
    //hBoxGalaxy->addWidget(new QLabel("Coordinates: "));
    hBoxGalaxy->addWidget(galaxyCoord);
    QBoxLayout* vBoxGalaxy = new QVBoxLayout;
    vBoxGalaxy->addWidget(new QLabel("Galaxy:"));
    vBoxGalaxy->addLayout(hBoxGalaxy);

    QBoxLayout* hBoxPlanet = new QHBoxLayout;
    hBoxPlanet->addWidget(planetBox);
    //hBoxPlanet->addWidget(new QLabel("Coordinates: "));
    hBoxPlanet->addWidget(planetCoord);
    QBoxLayout* vBoxPlanet = new QVBoxLayout;
    vBoxPlanet->addWidget(new QLabel("Planet:"));
    vBoxPlanet->addLayout(hBoxPlanet);

    QBoxLayout* vBoxPlanetInfo = new QVBoxLayout;
    QBoxLayout* hBox;
    hBox = new QHBoxLayout;
    hBox->addWidget(new QLabel("Energy potential: "));
    hBox->addWidget(energyPotential);
    vBoxPlanetInfo->addLayout(hBox);
    hBox = new QHBoxLayout;
    hBox->addWidget(new QLabel("User: "));
    hBox->addWidget(user);
    vBoxPlanetInfo->addLayout(hBox);

    QBoxLayout* buttonLay = new QHBoxLayout;
    buttonLay->addWidget(colonizationButton);
    buttonLay->addWidget(pillageButton);
    buttonLay->addWidget(transportButton);

    QBoxLayout* layout = new QVBoxLayout;
    layout->addLayout(vBoxGalaxy);
    layout->addLayout(vBoxPlanet);
    layout->addLayout(vBoxPlanetInfo);
    layout->addLayout(buttonLay);
    setLayout(layout);

    //Load galaxy
    QSqlQuery query = DataBase::getGalaxyList();
    while (query.next()) {
        galaxyBox->addItem(query.record().value("name").toString());
    }
    galaxyBox->setCurrentIndex(galaxyBox->findText(planet.galaxy));
    connect(galaxyBox, SIGNAL(currentIndexChanged(int)), this, SLOT(galaxyChangedSlot(int)));
    galaxyChangedSlot(galaxyBox->currentIndex());

    connect(colonizationButton, SIGNAL(clicked()), this, SLOT(colonizationSlot()));
    connect(pillageButton, SIGNAL(clicked()), this, SLOT(pillageSlot()));
    connect(transportButton, SIGNAL(clicked()), this, SLOT(transportSlot()));
}

void UniverseWidget::planetUpdate(const PlanetRecord &)
{
    galaxyBox->setCurrentIndex(galaxyBox->findText(getPlanet().galaxy));
}

void UniverseWidget::galaxyChangedSlot(int index)
{
    galaxy = DataBase::getGalaxy(galaxyBox->itemText(index));
    disconnect(planetBox, SIGNAL(currentIndexChanged(int)), this, SLOT(planetChangedSlot(int)));
    planetBox->clear();

    QSqlQuery query = DataBase::getPlanetListByGalaxy(galaxy.name);
    while (query.next()) {
        planetBox->addItem(query.record().value("name").toString());
    }

    planetBox->setCurrentIndex(0);
    connect(planetBox, SIGNAL(currentIndexChanged(int)), this, SLOT(planetChangedSlot(int)));
    planetChangedSlot(planetBox->currentIndex());

    galaxyCoord->setText(QString::number(galaxy.coord.x) + " / " + QString::number(galaxy.coord.y) + " / " + QString::number(galaxy.coord.z));
}

void UniverseWidget::planetChangedSlot(int index)
{
    planet = DataBase::getPlanet(planetBox->itemText(index));
    planetCoord->setText(QString::number(planet.coord.x) + " / " + QString::number(planet.coord.y) + " / " + QString::number(planet.coord.z));
    energyPotential->setText(QString::number(planet.energyPotential));
    user->setText(planet.user);

    if (planet.name == getPlanet().name) {
        colonizationButton->setEnabled(false);
        pillageButton->setEnabled(false);
        transportButton->setEnabled(false);
    } else {
        colonizationButton->setEnabled(planet.user.isEmpty() ? true : false);
        pillageButton->setEnabled(planet.user.isEmpty() || planet.user == getPlanet().user ? false : true);
        transportButton->setEnabled(planet.user.isEmpty() ? false : true);
    }
}

void UniverseWidget::colonizationSlot()
{
    QWidget* widget = new AddMissionWidget(DataBase::calculateMission(getPlanet().name, planet.name), AddMissionWidget::COLONIZATION, getPlanet().deuter, getPlanet().metal);
    widget->show();
}

void UniverseWidget::pillageSlot()
{
    QWidget* widget = new AddMissionWidget(DataBase::calculateMission(getPlanet().name, planet.name), AddMissionWidget::PILLAGE, getPlanet().deuter, getPlanet().metal);
    widget->show();
}

void UniverseWidget::transportSlot()
{
    QWidget* widget = new AddMissionWidget(DataBase::calculateMission(getPlanet().name, planet.name), AddMissionWidget::TRANSPORT, getPlanet().deuter, getPlanet().metal);
    widget->show();
}
