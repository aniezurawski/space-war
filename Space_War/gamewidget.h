#ifndef GAMEWIDGET_H
#define GAMEWIDGET_H

#include "planetrecord.h"
#include "mainwidget.h"
#include <QWidget>
#include <QTabBar>
#include <QComboBox>
#include <QLabel>
#include <QGroupBox>
#include <QPushButton>
#include <QTimer>

class GameWidget : public QGroupBox
{
    Q_OBJECT

    static const int SERVER_DATA_UPDATE_TIME = 500;

    static MainWidget* createBuildingWidget(const PlanetRecord& planet);
    static MainWidget* createShipyardWidget(const PlanetRecord& planet);
    static MainWidget* createMissionWidget(const PlanetRecord& planet);
    static MainWidget* createUniverseWidget(const PlanetRecord& planet);
    static const int MAIN_WIDGET_NUMBER = 4;
    static const QString MAIN_WIDGET_NAME[MAIN_WIDGET_NUMBER];
    static MainWidget* (*const MAIN_WIDGET_CREATOR[MAIN_WIDGET_NUMBER])(const PlanetRecord& planet);

public:
    explicit GameWidget(const QString& login, QWidget *parent = 0);
    
signals:
    
public slots:
    void tabChangedSlot(int index);
    void planetChangedSlot(int index);
    void refreshSlot();
    void serverUpdateDataTimerSlot();

private:
    QComboBox* planetBox;
    QLabel* planetCoordLabel;
    QLabel* galaxyLabel;
    QLabel* galaxyCoordLabel;
    QPushButton* refreshButton;
    QLabel* metalLabel;
    QLabel* deuterLabel;
    QLabel* energyInfoLabel;
    QLabel* energyPotentialLabel;
    QTabBar* tabBar;
    QLayout* mainLayout;
    MainWidget* mainWidget;

    QString login;
    PlanetRecord planet;

    QTimer serverUpdateDataTimer;

};

#endif // GAMEWIDGET_H
