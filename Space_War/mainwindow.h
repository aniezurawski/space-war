#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "database.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT


    
public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void loginSlot(const QString& login);

private:
    DataBase db;

};

#endif // MAINWINDOW_H
