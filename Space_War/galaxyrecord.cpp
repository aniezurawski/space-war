#include "galaxyrecord.h"

GalaxyRecord::GalaxyRecord(const QSqlRecord &record) :
    name(record.value("name").toString()),
    coord(record.value("coord_x").toInt(), record.value("coord_y").toInt(), record.value("coord_z").toInt())
{
}
