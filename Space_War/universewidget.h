#ifndef UNIVERSEWIDGET_H
#define UNIVERSEWIDGET_H

#include "mainwidget.h"
#include "galaxyrecord.h"
#include <QComboBox>
#include <QLabel>
#include <QLayout>
#include <QPushButton>

class UniverseWidget : public MainWidget
{
    Q_OBJECT
public:
    explicit UniverseWidget(const PlanetRecord& planet, QWidget *parent = 0);
    virtual void planetUpdate(const PlanetRecord &);
    
signals:
    
public slots:
    void galaxyChangedSlot(int index);
    void planetChangedSlot(int index);
    void colonizationSlot();
    void pillageSlot();
    void transportSlot();

private:
    GalaxyRecord galaxy;
    PlanetRecord planet;

    QComboBox* galaxyBox;
    QLabel* galaxyCoord;
    QComboBox* planetBox;
    QLabel* planetCoord;
    QLabel* energyPotential;
    QLabel* user;
    QPushButton* colonizationButton;
    QPushButton* pillageButton;
    QPushButton* transportButton;
    
};

#endif // UNIVERSEWIDGET_H
