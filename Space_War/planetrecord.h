#ifndef PLANETRECORD_H
#define PLANETRECORD_H

#include <QtSql>
#include "point3.h"

class PlanetRecord
{
public:
    PlanetRecord() {}
    PlanetRecord(const QSqlRecord& record);

    void recalculateEnergy();

    QString name;
    Point3i coord;
    int energyPotential;
    int metal;
    int deuter;
    QString user;
    QString galaxy;

    int energyProduction;
    int energyRequest;
};

#endif // PLANETRECORD_H
