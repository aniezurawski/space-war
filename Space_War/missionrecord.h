#ifndef MISSIONRECORD_H
#define MISSIONRECORD_H

#include <QtSql>
#include <map>

class MissionRecord
{
public:
    MissionRecord();
    MissionRecord(const QSqlRecord &record);
    void recalculateShips();

    int id;
    QString type;
    QString target;
    int time;
    int progress;
    bool ret;
    int cargo;
    std::vector<QString> shipName;
    std::vector<int> shipCount;
};

#endif // MISSIONRECORD_H
