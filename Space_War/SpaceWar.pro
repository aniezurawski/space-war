#-------------------------------------------------
#
# Project created by QtCreator 2012-12-28T18:51:33
#
#-------------------------------------------------

QT       += core gui
QT       += sql

TARGET = SpaceWar
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    loginwidget.cpp \
    database.cpp \
    gamewidget.cpp \
    planetrecord.cpp \
    buildingwidget.cpp \
    missionswidget.cpp \
    mainwidget.cpp \
    galaxyrecord.cpp \
    universewidget.cpp \
    shipyardwidget.cpp \
    buildingrecord.cpp \
    missionrecord.cpp \
    addmissionwidget.cpp

HEADERS  += mainwindow.h \
    loginwidget.h \
    database.h \
    point3.h \
    gamewidget.h \
    planetrecord.h \
    buildingwidget.h \
    missionswidget.h \
    mainwidget.h \
    galaxyrecord.h \
    universewidget.h \
    shipyardwidget.h \
    buildingrecord.h \
    missionrecord.h \
    addmissionwidget.h
