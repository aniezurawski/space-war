#include "buildingrecord.h"

BuildingRecord::BuildingRecord(const QSqlRecord &record) :
    type(record.value("real_type").toString()),
    exists(!record.value("planet").isNull()),
    planet(record.value("planet").toString()),
    level(record.value("level").toInt()),
    isActive(record.value("is_active").toBool()),
    constructionProgress(record.value("construction_progress").toInt()),
    constructionEnergyRequest(record.value("construction_energy_request").toInt()),
    workingEnergyRequest(record.value("working_energy_request").toInt()),
    constructionTime(record.value("construction_time").toInt()),
    metalCost(record.value("metal_cost").toInt())
{
}
