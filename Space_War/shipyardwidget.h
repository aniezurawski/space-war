#ifndef SHIPYARDWIDGET_H
#define SHIPYARDWIDGET_H

#include "mainwidget.h"
#include <vector>
#include <QLabel>
#include <QPushButton>

class ShipyardWidget : public MainWidget
{
    Q_OBJECT
public:
    explicit ShipyardWidget(const PlanetRecord& planet, QWidget *parent = 0);
    virtual void planetUpdate(const PlanetRecord &planet);
    
signals:
    
public slots:
    void buildClickedSlot(int id);

private:
    std::vector<QLabel*> shipNameLabel;
    std::vector<QLabel*> shipStationedLabel;
    std::vector<QLabel*> shipMissionLabel;
    std::vector<QLabel*> shipArmourLabel;
    std::vector<QLabel*> shipAttackLabel;
    std::vector<QLabel*> shipCapacityLabel;
    std::vector<QLabel*> shipCostLabel;
    std::vector<QLabel*> shipConstructionLabel;
    std::vector<QPushButton*> shipAddButton;

};

#endif // SHIPYARDWIDGET_H
