#ifndef LOGINWIDGET_H
#define LOGINWIDGET_H

#include <QWidget>
#include <QLabel>
#include <QLineEdit>
#include <QCheckBox>
#include <QPushButton>

class LoginWidget : public QWidget
{
    Q_OBJECT
public:
    explicit LoginWidget(QWidget *parent = 0);
    
signals:
    void loginSignal(const QString& login);
    
public slots:
    void buttonSlot();
    void newUserCheckBoxChangeStateSlot(int state);

private:
    QCheckBox* newUserCheckBox;
    QLineEdit* loginBox;
    QLineEdit* passwordBox;
    QLineEdit* repeatPassBox;
    QPushButton* button;
    QLabel* errorLabel;
    
    void login();
    void registration();

    bool isLoginCorrect();
};

#endif // LOGINWIDGET_H
