#ifndef ADDMISSIONWIDGET_H
#define ADDMISSIONWIDGET_H

#include "planetrecord.h"
#include "galaxyrecord.h"
#include <vector>
#include <QGroupBox>
#include <QLabel>
#include <QSpinBox>
#include <QPushButton>

class AddMissionWidget : public QGroupBox
{
    Q_OBJECT
public:
    static const int COLONIZATION = 0;
    static const int PILLAGE = 1;
    static const int TRANSPORT = 2;
    static const QString MISSION_TYPE_NAME[3];

    explicit AddMissionWidget(const QSqlRecord& record, int type, int deuter, int metal, QWidget *parent = 0);
    
signals:
    
public slots:
    void recalculateSlot(int);
    void startMissionSlot();

private:
    QString homePlanet;
    QString targetPlanet;
    QString homeGalaxy;
    QString targetGalaxy;
    int time;
    int deuterPerShip;
    int type;
    int deuter;
    QSpinBox* cargoSpinBox;
    QLabel* cargoMaxLabel;

    int totalDeuter;
    QLabel* totalDeuterLabel;
    std::vector<QString> shipName;
    std::vector<QSpinBox*> shipCount;
    std::vector<QLabel*> shipMax;
    std::vector<int> shipCapacity;
    
    QPushButton* buttonStart;

};

#endif // ADDMISSIONWIDGET_H
