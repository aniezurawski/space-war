#include "addmissionwidget.h"
#include "database.h"
#include <QLayout>
#include <QLabel>

const QString AddMissionWidget::MISSION_TYPE_NAME[] = {
    "Colonization",
    "Pillage",
    "Transport"
};

AddMissionWidget::AddMissionWidget(const QSqlRecord &record, int type, int deuter, int metal, QWidget *parent) :
    QGroupBox(MISSION_TYPE_NAME[type], parent),
    homePlanet(record.value("home_planet").toString()),
    targetPlanet(record.value("target_planet").toString()),
    homeGalaxy(record.value("home_galaxy").toString()),
    targetGalaxy(record.value("target_galaxy").toString()),
    time(record.value("time").toInt()),
    deuterPerShip(record.value("deuter").toInt()),
    type(type),
    deuter(deuter),
    cargoSpinBox(new QSpinBox),
    cargoMaxLabel(new QLabel),
    totalDeuter(0),
    totalDeuterLabel(new QLabel),
    shipName(DataBase::getShipTypeCount()),
    shipCount(shipName.size()),
    shipMax(shipName.size()),
    shipCapacity(shipName.size()),
    buttonStart(new QPushButton("Start!"))
{
    cargoSpinBox->setValue(0);
    cargoSpinBox->setMaximum(metal);

    QBoxLayout* labelLay = new QVBoxLayout;
    labelLay->addWidget(new QLabel("Home palnet: "));
    labelLay->addWidget(new QLabel("Target palnet: "));
    labelLay->addWidget(new QLabel("Time: "));
    labelLay->addWidget(new QLabel("Required deuterium (per ship): "));
    labelLay->addWidget(new QLabel("Required deuterium (total): "));
    labelLay->addWidget(new QLabel("Cargo: "));

    QBoxLayout* valueLay = new QVBoxLayout;
    valueLay->addWidget(new QLabel(homePlanet + " (in " + homeGalaxy + ")"));
    valueLay->addWidget(new QLabel(targetPlanet + " (in " + targetGalaxy + ")"));
    valueLay->addWidget(new QLabel(QString::number(time)));
    valueLay->addWidget(new QLabel(QString::number(deuterPerShip)));
    valueLay->addWidget(totalDeuterLabel);
    QBoxLayout* cargoLay = new QHBoxLayout;
    cargoLay->addWidget(cargoSpinBox);
    cargoLay->addWidget(cargoMaxLabel);
    valueLay->addLayout(cargoLay);

    QBoxLayout* paramLay = new QHBoxLayout;
    paramLay->addLayout(labelLay);
    paramLay->addLayout(valueLay);

    QBoxLayout* shipNameLay = new QVBoxLayout;
    QBoxLayout* shipCountLay = new QVBoxLayout;
    QBoxLayout* shipMaxLay = new QVBoxLayout;
    QSqlQuery query = DataBase::getShipTypeList(homePlanet);
    for (unsigned int i = 0; query.next(); ++i) {
        shipName[i] = query.record().value("type").toString();
        shipNameLay->addWidget(new QLabel(shipName[i]));
        shipCount[i] = new QSpinBox();
        shipCountLay->addWidget(shipCount[i]);
        shipCount[i]->setValue(0);
        shipCount[i]->setMaximum(query.record().value("stationed").toInt());
	shipCapacity[i] = query.record().value("capacity").toInt();
        shipMaxLay->addWidget(new QLabel(QString(" / ") + QString::number(shipCount[i]->maximum())));
        connect(shipCount[i], SIGNAL(valueChanged(int)), this, SLOT(recalculateSlot(int)));
    }

    QGroupBox* fleetGroup = new QGroupBox("Fleet");
    QHBoxLayout* fleetLey = new QHBoxLayout;
    fleetLey->addLayout(shipNameLay);
    fleetLey->addLayout(shipCountLay);
    fleetLey->addLayout(shipMaxLay);
    fleetGroup->setLayout(fleetLey);

    QVBoxLayout* layout = new QVBoxLayout;
    layout->addLayout(paramLay);
    layout->addWidget(fleetGroup);
    layout->addWidget(buttonStart);

    setLayout(layout);
    setWindowModality(Qt::ApplicationModal);

    recalculateSlot(0);
    connect(buttonStart, SIGNAL(clicked()), this, SLOT(startMissionSlot()));
}

void AddMissionWidget::recalculateSlot(int)
{
    totalDeuter = 0;
    int totalShipCount = 0;
    int capacity = 0;
    for (unsigned int i = 0; i < shipCount.size(); ++i) {
        totalShipCount += shipCount[i]->value();
	capacity += shipCount[i]->value() * shipCapacity[i];
    }
    totalDeuter = totalShipCount * deuterPerShip;
    totalDeuterLabel->setText(QString::number(totalDeuter));
    cargoMaxLabel->setText(QString(" / ") + QString::number(capacity));
    cargoSpinBox->setMaximum(capacity);

    if (totalDeuter <= deuter && totalShipCount > 0) {
        buttonStart->setEnabled(true);
    } else {
        buttonStart->setEnabled(false);
    }
}

void AddMissionWidget::startMissionSlot()
{
    std::vector<int> count(shipCount.size());
    for (unsigned int i = 0; i < shipCount.size(); ++i) {
        count[i] = shipCount[i]->value();
    }

    DataBase::startMission(MISSION_TYPE_NAME[type], homePlanet, targetPlanet, shipName, count, cargoSpinBox->value());
    close();
}
