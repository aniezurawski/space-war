#include "gamewidget.h"
#include "database.h"
#include "missionswidget.h"
#include "universewidget.h"
#include "buildingwidget.h"
#include "shipyardwidget.h"
#include "galaxyrecord.h"
#include <QLayout>
#include <cstdio>
#include <QDebug>
#include <QGroupBox>
#include <QGridLayout>
#include <QtSql>

const QString GameWidget::MAIN_WIDGET_NAME[] = {
    "Buildings",
    "Shipyard",
    "Missions",
    "Universe"
};

MainWidget* (*const GameWidget::MAIN_WIDGET_CREATOR[])(const PlanetRecord& planet) = {
    GameWidget::createBuildingWidget,
    GameWidget::createShipyardWidget,
    GameWidget::createMissionWidget,
    GameWidget::createUniverseWidget
};

GameWidget::GameWidget(const QString &login, QWidget *parent) :
    QGroupBox(login, parent),
    planetBox(new QComboBox),
    planetCoordLabel(new QLabel),
    galaxyLabel(new QLabel),
    galaxyCoordLabel(new QLabel),
    refreshButton(new QPushButton("Refresh")),
    metalLabel(new QLabel),
    deuterLabel(new QLabel),
    energyInfoLabel(new QLabel),
    energyPotentialLabel(new QLabel),
    tabBar(new QTabBar),
    mainLayout(new QVBoxLayout),
    mainWidget(NULL),
    login(login)
{
    QBoxLayout* pgLabelLay = new QVBoxLayout;
    pgLabelLay->addWidget(new QLabel("Planet: "));
    pgLabelLay->addWidget(new QLabel("Galaxy: "));
    QBoxLayout* pgNameLay = new QVBoxLayout;
    pgNameLay->addWidget(planetBox);
    pgNameLay->addWidget(galaxyLabel);
    QBoxLayout* pgCoordLay = new QVBoxLayout;
    pgCoordLay->addWidget(planetCoordLabel);
    pgCoordLay->addWidget(galaxyCoordLabel);

    QBoxLayout* pgLayout = new QHBoxLayout;
    pgLayout->addLayout(pgLabelLay);
    pgLayout->addLayout(pgNameLay);
    pgLayout->addLayout(pgCoordLay);
    pgLayout->addWidget(new QLabel);
    pgLayout->addWidget(refreshButton);
    connect(refreshButton, SIGNAL(clicked()), this, SLOT(refreshSlot()));
    refreshSlot();

    QBoxLayout* statsName1 = new QVBoxLayout;
    statsName1->addWidget(new QLabel("Metal: "));
    statsName1->addWidget(new QLabel("Deuterium: "));

    QBoxLayout* statsName2 = new QVBoxLayout;
    statsName2->addWidget(new QLabel("Energy potential: "));
    statsName2->addWidget(new QLabel("Energy: "));

    QBoxLayout* statsValue1 = new QVBoxLayout;
    statsValue1->addWidget(metalLabel);
    statsValue1->addWidget(deuterLabel);

    QBoxLayout* statsValue2 = new QVBoxLayout;
    statsValue2->addWidget(energyPotentialLabel);
    statsValue2->addWidget(energyInfoLabel);

    QBoxLayout* statsLayout = new QHBoxLayout;
    statsLayout->addLayout(statsName1);
    statsLayout->addLayout(statsValue1);
    statsLayout->addLayout(statsName2);
    statsLayout->addLayout(statsValue2);
    QGroupBox* statsGroup = new QGroupBox("Planet stats:");
    statsGroup->setLayout(statsLayout);

    for (int i = 0; i < MAIN_WIDGET_NUMBER; i++) {
        tabBar->addTab(MAIN_WIDGET_NAME[i]);
    }
    tabBar->setCurrentIndex(0);
    connect(tabBar, SIGNAL(currentChanged(int)), this, SLOT(tabChangedSlot(int)));
    tabChangedSlot(0);

    QBoxLayout* layoutUp = new QVBoxLayout;
    layoutUp->addLayout(pgLayout);
    layoutUp->addWidget(statsGroup);
    layoutUp->addWidget(tabBar);
    QBoxLayout* layout = new QVBoxLayout;
    setLayout(layout);
    layout->addLayout(layoutUp);
    layout->addLayout(mainLayout);

    //connect(&serverUpdateDataTimer, SIGNAL(timeout()), this, SLOT(serverUpdateDataTimerSlot()));
    //serverUpdateDataTimerSlot();
}

void GameWidget::tabChangedSlot(int index)
{
    delete mainWidget;
    mainWidget = MAIN_WIDGET_CREATOR[index](planet);
    mainLayout->addWidget(mainWidget);
    connect(mainWidget, SIGNAL(refresh()), this, SLOT(refreshSlot()));
}

void GameWidget::refreshSlot()
{
    disconnect(planetBox, SIGNAL(currentIndexChanged(int)), this, SLOT(planetChangedSlot(int)));
    planetBox->clear();

    QSqlQuery query = DataBase::getPlanetListByUser(login);
    while (query.next()) {
        planetBox->addItem(query.record().value("name").toString());
    }

    if (!planet.name.isEmpty())
        planetBox->setCurrentIndex(planetBox->findText(planet.name));
    else
        planetBox->setCurrentIndex(0);
    connect(planetBox, SIGNAL(currentIndexChanged(int)), this, SLOT(planetChangedSlot(int)));
    planetChangedSlot(planetBox->currentIndex());
}

void GameWidget::planetChangedSlot(int index)
{
    planet = DataBase::getPlanet(planetBox->itemText(index));
    planetCoordLabel->setText(QString(" ( ") + QString::number(planet.coord.x) + " / " + QString::number(planet.coord.y) + " / " + QString::number(planet.coord.z) + " ) ");

    GalaxyRecord galaxy = DataBase::getGalaxy(planet.galaxy);
    galaxyCoordLabel->setText(QString(" ( ") + QString::number(galaxy.coord.x) + " / " + QString::number(galaxy.coord.y) + " / " + QString::number(galaxy.coord.z) + " ) ");
    galaxyLabel->setText(planet.galaxy);

    metalLabel->setText(QString::number(planet.metal));
    deuterLabel->setText(QString::number(planet.deuter));
    energyInfoLabel->setText(QString::number(planet.energyRequest) + "/" + QString::number(planet.energyProduction));
    energyPotentialLabel->setText(QString::number(planet.energyPotential));
    if (mainWidget)
        mainWidget->setPlanet(planet);
}

MainWidget* GameWidget::createBuildingWidget(const PlanetRecord& planet)
{
    return new BuildingWidget(planet);
}

MainWidget* GameWidget::createShipyardWidget(const PlanetRecord& planet)
{
    return new ShipyardWidget(planet);
}

MainWidget* GameWidget::createMissionWidget(const PlanetRecord& planet)
{
    return new MissionsWidget(planet);
}

MainWidget* GameWidget::createUniverseWidget(const PlanetRecord& planet)
{
    return new UniverseWidget(planet);
}

void GameWidget::serverUpdateDataTimerSlot()
{
    DataBase::serverUpdateData();
    serverUpdateDataTimer.start(SERVER_DATA_UPDATE_TIME);
}
