#ifndef BUILDINGWIDGET_H
#define BUILDINGWIDGET_H

#include "mainwidget.h"
#include "buildingrecord.h"
#include <QPushButton>
#include <QGroupBox>
#include <QListWidget>
#include <QListWidgetItem>
#include <QLabel>

class BuildingWidget : public MainWidget
{
    Q_OBJECT

public:
    explicit BuildingWidget(const PlanetRecord& planet, QWidget *parent = 0);
    virtual void planetUpdate(const PlanetRecord &);
    
signals:
    
public slots:
    void buildingChangedSlot(QListWidgetItem * current, QListWidgetItem*);
    void buildClickedSlot();
    void activeClickedSlot();

private:
    BuildingRecord building;

    QListWidget* buildingList;
    QGroupBox* buildingOption;
    QLabel* constructionEnergyRequestLabel;
    QLabel* workingEnergyRequestLabel;
    QLabel* metalCostLabel;
    QLabel* levelLabel;
    QLabel* constructionProgressLabel;

    QPushButton* buildButton;
    QPushButton* activeButton;

    bool buildPossible();
    bool activePossible();
};

#endif // BUILDINGWIDGET_H
