#include "buildingwidget.h"
#include"database.h"
#include <QLayout>
#include <QtSql>

BuildingWidget::BuildingWidget(const PlanetRecord& planet, QWidget *parent) :
    MainWidget("Buildings", planet, parent),
    buildingList(new QListWidget),
    buildingOption(new QGroupBox),
    constructionEnergyRequestLabel(new QLabel),
    workingEnergyRequestLabel(new QLabel),
    metalCostLabel(new QLabel),
    levelLabel(new QLabel),
    constructionProgressLabel(new QLabel),
    buildButton(new QPushButton),
    activeButton(new QPushButton)
{
    QBoxLayout* constructionEnergyDemandLay = new QHBoxLayout;
    constructionEnergyDemandLay->addWidget(new QLabel("\tConstruction: "));
    constructionEnergyDemandLay->addWidget(constructionEnergyRequestLabel);

    QBoxLayout* workingEnergyDemandLay = new QHBoxLayout;
    workingEnergyDemandLay->addWidget(new QLabel("\tWorking: "));
    workingEnergyDemandLay->addWidget(workingEnergyRequestLabel);

    QBoxLayout* energyDemandLay = new QVBoxLayout;
    energyDemandLay->addWidget(new QLabel("Energy demand"));
    energyDemandLay->addLayout(constructionEnergyDemandLay);
    energyDemandLay->addLayout(workingEnergyDemandLay);

    QBoxLayout* metalCostLay = new QHBoxLayout;
    metalCostLay->addWidget(new QLabel("Required metal: "));
    metalCostLay->addWidget(metalCostLabel);

    QBoxLayout* levelLay = new QHBoxLayout;
    levelLay->addWidget(new QLabel("Level: "));
    levelLay->addWidget(levelLabel);

    QBoxLayout* constructionProgressLay = new QHBoxLayout;
    constructionProgressLay->addWidget(new QLabel("Construction progress: "));
    constructionProgressLay->addWidget(constructionProgressLabel);

    QBoxLayout* optionLay = new QVBoxLayout;
    optionLay->addLayout(energyDemandLay);
    optionLay->addLayout(metalCostLay);
    optionLay->addLayout(levelLay);
    optionLay->addLayout(constructionProgressLay);
    optionLay->addWidget(buildButton);
    optionLay->addWidget(activeButton);
    buildingOption->setLayout(optionLay);

    QGridLayout* layout = new QGridLayout;
    layout->addWidget(buildingList, 0, 0, Qt::AlignLeft);
    layout->addWidget(buildingOption, 0, 1, Qt::AlignLeft);
    setLayout(layout);

    buildingList->setSelectionMode(QListWidget::SingleSelection);
    buildingList->setFixedWidth(200);

    QSqlQuery query = DataBase::getBuildingTypeList();
    while (query.next()) {
        buildingList->addItem(new QListWidgetItem(query.record().value("type").toString()));
    }
    buildingList->setCurrentRow(0);
    connect(buildingList, SIGNAL(currentItemChanged(QListWidgetItem*,QListWidgetItem*)), this, SLOT(buildingChangedSlot(QListWidgetItem*,QListWidgetItem*)));
    buildingChangedSlot(buildingList->currentItem(), NULL);

    connect(buildButton, SIGNAL(clicked()), this, SLOT(buildClickedSlot()));
    connect(activeButton, SIGNAL(clicked()), this, SLOT(activeClickedSlot()));
}

void BuildingWidget::planetUpdate(const PlanetRecord &)
{
    buildingChangedSlot(buildingList->currentItem(), NULL);
}

void BuildingWidget::buildingChangedSlot(QListWidgetItem *current, QListWidgetItem *)
{
    building = DataBase::getBuilding(current->text(), getPlanet().name);
    buildingOption->setTitle(building.type + (building.isActive || !building.exists ? "" : " (Inactive)") + (!building.isActive || building.constructionProgress == 0 || !building.exists ? "" : " (During construction)"));
    constructionEnergyRequestLabel->setText(QString::number(building.constructionEnergyRequest));
    workingEnergyRequestLabel->setText(QString::number(building.workingEnergyRequest));
    metalCostLabel->setText(QString::number(building.metalCost));
    levelLabel->setText(QString::number(building.level));
    constructionProgressLabel->setText((building.exists ? QString::number(building.constructionTime - building.constructionProgress) : "0") + " / " + QString::number(building.constructionTime));

    buildButton->setText((building.level == 0) ? "Build" : "Upgrade");
    activeButton->setText((building.isActive || !building.exists) ? "Deactivate" : "Activate");

    if (buildPossible())
        buildButton->setEnabled(true);
    else
        buildButton->setEnabled(false);

    if (activePossible())
        activeButton->setEnabled(true);
    else
        activeButton->setEnabled(false);
}

void BuildingWidget::buildClickedSlot()
{
    if (building.level == 0)
        DataBase::createBuilding(building.type, getPlanet().name);
    else
        DataBase::upgradeBuilding(building.type, getPlanet().name);
    buildingChangedSlot(buildingList->currentItem(), NULL);
    emit refresh();
}

void BuildingWidget::activeClickedSlot()
{
    DataBase::setBuildingActive(building.type, getPlanet().name, !building.isActive);
    emit refresh();
}

bool BuildingWidget::buildPossible()
{
    return getPlanet().metal >= building.metalCost
            && (!building.exists || building.constructionProgress == 0);
}

bool BuildingWidget::activePossible()
{
    return building.exists;
}
