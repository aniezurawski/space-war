--DROP LANGUAGE PLPGSQL;
--CREATE LANGUAGE PLPGSQL;

DROP FUNCTION random_range(INTEGER, INTEGER) CASCADE;
CREATE FUNCTION random_range(INTEGER, INTEGER) RETURNS BIGINT LANGUAGE PLPGSQL AS $$ BEGIN
	RETURN round(random() * ($2 - $1)) + $1;
END $$;

DROP FUNCTION get_max_coord() CASCADE;
CREATE FUNCTION get_max_coord() RETURNS INTEGER LANGUAGE PLPGSQL AS 'BEGIN RETURN 1000000; END';
DROP FUNCTION get_std_energy_potential() CASCADE;
CREATE FUNCTION get_std_energy_potential() RETURNS INTEGER LANGUAGE PLPGSQL AS 'BEGIN RETURN 500; END';
DROP FUNCTION get_start_metal() CASCADE;
CREATE FUNCTION get_start_metal() RETURNS INTEGER LANGUAGE PLPGSQL AS 'BEGIN RETURN 1000; END';
DROP FUNCTION get_metal_production() CASCADE;
CREATE FUNCTION get_metal_production() RETURNS INTEGER LANGUAGE PLPGSQL AS 'BEGIN RETURN 50; END';
DROP FUNCTION get_deuter_production() CASCADE;
CREATE FUNCTION get_deuter_production() RETURNS INTEGER LANGUAGE PLPGSQL AS 'BEGIN RETURN 50; END';

DROP FUNCTION get_shipyard_name() CASCADE;
CREATE FUNCTION get_shipyard_name() RETURNS VARCHAR(50) LANGUAGE PLPGSQL AS $$ BEGIN RETURN 'Shipyard'; END $$;
DROP FUNCTION get_powerstation_name() CASCADE;
CREATE FUNCTION get_powerstation_name() RETURNS VARCHAR(50) LANGUAGE PLPGSQL AS $$ BEGIN RETURN 'Solar plant'; END $$;
DROP FUNCTION get_metalmine_name() CASCADE;
CREATE FUNCTION get_metalmine_name() RETURNS VARCHAR(50) LANGUAGE PLPGSQL AS $$ BEGIN RETURN 'Metal mine'; END $$;
DROP FUNCTION get_deutermine_name() CASCADE;
CREATE FUNCTION get_deutermine_name() RETURNS VARCHAR(50) LANGUAGE PLPGSQL AS $$ BEGIN RETURN 'Deuterium extractor'; END $$;

DROP FUNCTION get_colonization_name() CASCADE;
CREATE FUNCTION get_colonization_name() RETURNS VARCHAR(50) LANGUAGE PLPGSQL AS $$ BEGIN RETURN 'Colonization'; END $$;
DROP FUNCTION get_transport_name() CASCADE;
CREATE FUNCTION get_transport_name() RETURNS VARCHAR(50) LANGUAGE PLPGSQL AS $$ BEGIN RETURN 'Transport'; END $$;
DROP FUNCTION get_pillage_name() CASCADE;
CREATE FUNCTION get_pillage_name() RETURNS VARCHAR(50) LANGUAGE PLPGSQL AS $$ BEGIN RETURN 'Pillage'; END $$;

DROP TABLE "user" CASCADE;
CREATE TABLE "user" (
	login VARCHAR(50) PRIMARY KEY NOT NULL,
	"password" VARCHAR(50) NOT NULL,
	active BOOLEAN NOT NULL DEFAULT true
);

DROP TABLE galaxy CASCADE;
CREATE TABLE galaxy (
	name VARCHAR(50) PRIMARY KEY,
	coord_x INTEGER NOT NULL CHECK (-get_max_coord() <= coord_x AND coord_x <= get_max_coord()),
	coord_y INTEGER NOT NULL CHECK (-get_max_coord() <= coord_y AND coord_y <= get_max_coord()),
	coord_z INTEGER NOT NULL CHECK (-get_max_coord() <= coord_z AND coord_z <= get_max_coord()),

	UNIQUE (coord_x, coord_y, coord_z)
);

DROP TABLE planet CASCADE;
CREATE TABLE planet (
	name VARCHAR(50) PRIMARY KEY,
	coord_x INTEGER NOT NULL CHECK (-get_max_coord() <= coord_x AND coord_x <= get_max_coord()),
	coord_y INTEGER NOT NULL CHECK (-get_max_coord() <= coord_y AND coord_y <= get_max_coord()),
	coord_z INTEGER NOT NULL CHECK (-get_max_coord() <= coord_z AND coord_z <= get_max_coord()),
	energy_potential INTEGER NOT NULL CHECK (energy_potential >= 0),
	metal INTEGER NOT NULL DEFAULT 0 CHECK (metal >= 0),
	deuter INTEGER NOT NULL DEFAULT 0 CHECK (deuter >= 0),
	"user" VARCHAR(50) REFERENCES "user" (login) ON UPDATE CASCADE ON DELETE SET NULL,
	galaxy VARCHAR(50) NOT NULL REFERENCES galaxy (name) ON UPDATE CASCADE ON DELETE CASCADE,

	UNIQUE (coord_x, coord_y, coord_z, galaxy)
);

DROP TABLE building_type CASCADE;
CREATE TABLE building_type (
	type VARCHAR(50) NOT NULL PRIMARY KEY,
	construction_energy_request INTEGER NOT NULL CHECK(construction_energy_request >= 0),
	working_energy_request INTEGER NOT NULL CHECK(working_energy_request >= 0),
	construction_time INTEGER NOT NULL CHECK(construction_time > 0),
	metal_cost INTEGER NOT NULL CHECK(metal_cost > 0)
);

DROP TABLE building CASCADE;
CREATE TABLE building (
	id SERIAL PRIMARY KEY,
	type VARCHAR(50) NOT NULL REFERENCES building_type (type) ON UPDATE CASCADE ON DELETE CASCADE,
	planet VARCHAR(50) NOT NULL REFERENCES planet (name) ON UPDATE CASCADE ON DELETE CASCADE,
	"level" INTEGER NOT NULL CHECK ("level" > 0),
	is_active BOOLEAN NOT NULL,
	construction_progress INTEGER NOT NULL CHECK(construction_progress >= 0),

	UNIQUE (type, planet)
);

DROP TABLE mission CASCADE;
CREATE TABLE mission (
	id INTEGER PRIMARY KEY,
	type VARCHAR(50) NOT NULL,
	target VARCHAR(50) NOT NULL REFERENCES planet (name) ON UPDATE CASCADE ON DELETE CASCADE,
	time INTEGER NOT NULL CHECK(time > 0),
	progress INTEGER NOT NULL CHECK(0 <= progress AND progress <= time),
	cargo INTEGER NOT NULL CHECK (cargo >= 0),
	return BOOLEAN NOT NULL
);

DROP TABLE ship_type CASCADE;
CREATE TABLE ship_type (
	type VARCHAR(50) PRIMARY KEY,
	armour INTEGER NOT NULL CHECK (armour > 0),
	attack INTEGER NOT NULL CHECK (attack >= 0),
	capacity INTEGER NOT NULL CHECK (capacity >= 0),
	construction_time INTEGER NOT NULL CHECK (construction_time > 0),
	metal_cost INTEGER NOT NULL CHECK (metal_cost > 0)
);

DROP TABLE ship CASCADE;
CREATE TABLE ship (
	id SERIAL PRIMARY KEY,
	type VARCHAR(50) NOT NULL REFERENCES ship_type (type) ON UPDATE CASCADE ON DELETE CASCADE,
	construction_progress INTEGER NOT NULL CHECK(construction_progress >= 0),
	home_planet VARCHAR(50) NOT NULL REFERENCES planet (name) ON UPDATE CASCADE ON DELETE CASCADE,
	mission INTEGER REFERENCES mission (id) ON UPDATE CASCADE ON DELETE CASCADE
);

--DROP VIEW ship_stationed CASCADE;
CREATE VIEW ship_stationed AS
	SELECT * FROM ship WHERE mission IS NULL;

--DROP VIEW ship_on_mission CASCADE;
CREATE VIEW ship_on_mission AS
	SELECT * FROM ship WHERE mission IS NOT NULL;

--DROP VIEW user_ship CASCADE;
CREATE VIEW user_ship AS
	SELECT p."user" AS "user", s.id AS ship
		FROM ship s LEFT JOIN planet p ON s.home_planet = p.name;

--DROP VIEW user_building CASCADE;
CREATE VIEW user_building AS
	SELECT p."user" AS "user", b.id AS building
		FROM building b LEFT JOIN planet p ON b.planet = p.name;

--DROP VIEW user_mission CASCADE;
CREATE VIEW user_mission AS
	SELECT us."user" AS "user", s.mission AS mission
		FROM ship s LEFT JOIN user_ship us ON s.id = us.ship
		GROUP BY s.mission, us.user
		HAVING s.mission IS NOT NULL;

--DROP VIEW building_under_construction
CREATE VIEW building_under_construction AS
	SELECT * FROM building WHERE construction_progress > 0 AND is_active;

--DROP VIEW building_working
CREATE VIEW building_working AS
	SELECT * FROM building WHERE construction_progress = 0 AND is_active;

--DROP VIEW building_energy_request CASCADE;
CREATE VIEW building_energy_request AS
	SELECT b.id, b.type, b.planet, t.construction_energy_request * b."level" AS energy_request
		FROM (SELECT * FROM building WHERE is_active AND construction_progress > 0) b
			LEFT JOIN building_type t ON b.type = t.type
	UNION ALL
	SELECT b.id, b.type, b.planet, t.working_energy_request * b."level" AS energy_request
		FROM (SELECT * FROM building WHERE is_active AND construction_progress = 0) b
			LEFT JOIN building_type t ON b.type = t.type;

--DROP VIEW planet_energy_request CASCADE;
CREATE VIEW planet_energy_request AS
	SELECT planet, sum(energy_request) AS energy_request
		FROM building_energy_request
		GROUP BY planet;

--DROP VIEW planet_energy_production CASCADE;
CREATE VIEW planet_energy_production AS
	SELECT p.name AS planet, e."level" * p.energy_potential AS energy_production
		FROM (SELECT * FROM building WHERE type = get_powerstation_name() AND is_active AND construction_progress = 0) e LEFT JOIN planet p ON p.name = e.planet;

--DROP VIEW planet_energy_ok CASCADE;
CREATE VIEW planet_energy_ok AS
	SELECT pr.name AS planet, COALESCE(pr.energy_request, 0) <= COALESCE(pp.energy_production, 0) AS ok
		FROM (SELECT p.name, pr.energy_request FROM planet p LEFT JOIN planet_energy_request pr ON p.name = pr.planet) pr
			LEFT JOIN planet_energy_production pp ON pr.name = pp.planet;

--DROP VIEW shipyard_ok CASCADE;
CREATE VIEW shipyard_ok AS
	SELECT p.name AS planet, COALESCE(s.is_active AND s.construction_progress = 0, FALSE) AND (SELECT ok FROM planet_energy_ok WHERE planet = p.name) AS ok
		FROM planet p LEFT JOIN (SELECT planet, is_active, construction_progress FROM building WHERE type = get_shipyard_name()) s ON p.name = s.planet;

DROP FUNCTION user_delete_procedure() CASCADE;
CREATE FUNCTION user_delete_procedure() RETURNS trigger LANGUAGE 'PLPGSQL' AS
$$ BEGIN
	DELETE FROM ship WHERE id IN (SELECT ship FROM user_ship WHERE "user" = OLD.id);
	DELETE FROM building WHERE id IN (SELECT building FROM user_building WHERE "user" = OLD.id);
	DELETE FROM mission WHERE id IN (SELECT mission FROM user_mission WHERE "user" = OLD.id);

	UPDATE planet SET "user" = NULL WHERE "user" = OLD.id;

	RETURN OLD;
END $$;

DROP FUNCTION user_create_procedure() CASCADE;
CREATE FUNCTION user_create_procedure() RETURNS trigger LANGUAGE 'PLPGSQL' AS
$$ BEGIN
	INSERT INTO planet (name, coord_x, coord_y, coord_z, energy_potential, metal, deuter, "user", galaxy)
		VALUES ('PU' || NEW.login,
			random_range(-get_max_coord(), get_max_coord()),
			random_range(-get_max_coord(), get_max_coord()),
			random_range(-get_max_coord(), get_max_coord()),
			get_std_energy_potential(),
			get_start_metal(),
			0,
			NEW.login,
			(SELECT name FROM galaxy
				ORDER BY random()
				LIMIT 1));

	RETURN NEW;
END $$;

DROP FUNCTION building_create_procedure() CASCADE;
CREATE FUNCTION building_create_procedure() RETURNS trigger LANGUAGE 'PLPGSQL' AS
$$ BEGIN
	UPDATE planet
		SET metal = metal - (SELECT metal_cost FROM building_type WHERE type = NEW.type)
		WHERE name = NEW.planet;

	RETURN NEW;
END $$;

DROP FUNCTION building_upgrade_procedure() CASCADE;
CREATE FUNCTION building_upgrade_procedure() RETURNS trigger LANGUAGE 'PLPGSQL' AS
$$ BEGIN
	UPDATE planet
		SET metal = metal - (SELECT metal_cost FROM building_type WHERE type = NEW.type)
		WHERE name = NEW.planet AND NEW."level" != OLD."level";

	RETURN NEW;
END $$;

DROP FUNCTION ship_create_procedure() CASCADE;
CREATE FUNCTION ship_create_procedure() RETURNS trigger LANGUAGE 'PLPGSQL' AS
$$ BEGIN
	UPDATE planet
		SET metal = metal - (SELECT metal_cost FROM ship_type WHERE type = NEW.type)
		WHERE name = NEW.home_planet;

	RETURN NEW;
END $$;

DROP FUNCTION mission_delete_procedure() CASCADE;
CREATE FUNCTION mission_delete_procedure() RETURNS trigger LANGUAGE 'PLPGSQL' AS
$$ BEGIN
	UPDATE planet
		SET metal = metal + OLD.cargo
		WHERE name = (SELECT home_planet FROM ship WHERE mission = OLD.id LIMIT 1);

	UPDATE ship SET mission = NULL WHERE mission = OLD.id;

	RETURN OLD;
END $$;

DROP FUNCTION calculate_mission(VARCHAR(50), VARCHAR(50)) CASCADE;
CREATE FUNCTION calculate_mission(VARCHAR(50), VARCHAR(50)) RETURNS TABLE("time" INTEGER, deuter INTEGER) LANGUAGE SQL AS
$$
	WITH
		home_planet 	AS (SELECT * FROM planet WHERE name = $1),
		target_planet 	AS (SELECT * FROM planet WHERE name = $2),
		home_galaxy	AS (SELECT * FROM galaxy WHERE name = (SELECT galaxy FROM home_planet)),
		target_galaxy	AS (SELECT * FROM galaxy WHERE name = (SELECT galaxy FROM target_planet)),
		dist 		AS (SELECT sqrt(pow((SELECT coord_x FROM home_planet) - (SELECT coord_x FROM target_planet), 2)
					+ pow((SELECT coord_y FROM home_planet) - (SELECT coord_y FROM target_planet), 2)
					+ pow((SELECT coord_z FROM home_planet) - (SELECT coord_z FROM target_planet), 2))
				+ 10 * sqrt(pow((SELECT coord_x FROM home_galaxy) - (SELECT coord_x FROM target_galaxy), 2)
					+ pow((SELECT coord_y FROM home_galaxy) - (SELECT coord_y FROM target_galaxy), 2)
					+ pow((SELECT coord_z FROM home_galaxy) - (SELECT coord_z FROM target_galaxy), 2)) AS dist)
		SELECT CAST((SELECT dist FROM dist) / 15000 AS INTEGER), CAST((SELECT dist FROM dist) / 5000 AS INTEGER);
$$;

DROP FUNCTION update_data() CASCADE;
CREATE FUNCTION update_data() RETURNS void LANGUAGE 'PLPGSQL' AS
$$ BEGIN
	UPDATE planet SET
		metal = metal + metal_production.production
		FROM planet_energy_ok energy_ok,
			(SELECT p.name planet,
				COALESCE(b."level"*get_metal_production()*(CASE WHEN (b.is_active AND b.construction_progress = 0) THEN 1 ELSE 0 END), 0) production
				FROM planet p LEFT JOIN (SELECT * FROM building WHERE type = get_metalmine_name()) b ON p.name = b.planet) metal_production
		WHERE planet."user" IS NOT NULL
			AND planet.name = metal_production.planet
			AND planet.name = energy_ok.planet
			AND energy_ok.ok
			AND metal_production.production > 0;

	UPDATE planet SET
		deuter = deuter + deuter_production.production
		FROM planet_energy_ok energy_ok,
			(SELECT p.name planet,
				COALESCE(b."level"*get_deuter_production()*(CASE WHEN (b.is_active AND b.construction_progress = 0) THEN 1 ELSE 0 END), 0) production
				FROM planet p LEFT JOIN (SELECT * FROM building WHERE type = get_deutermine_name()) b ON p.name = b.planet) deuter_production
		WHERE planet."user" IS NOT NULL
			AND planet.name = deuter_production.planet
			AND planet.name = energy_ok.planet
			AND energy_ok.ok
			AND deuter_production.production > 0;
	
	UPDATE building SET construction_progress = construction_progress - 1
		FROM planet_energy_ok energy_ok
		WHERE energy_ok.planet = building.planet AND energy_ok.ok AND construction_progress > 0 AND is_active;

	UPDATE ship SET construction_progress = construction_progress - 1
		FROM shipyard_ok
		WHERE shipyard_ok.planet = ship.home_planet AND shipyard_ok.ok AND construction_progress > 0;

	UPDATE mission SET progress = progress + 1;

	--COLONIZATION / TRANSPORT-- Update planet.user, planet.metal
	UPDATE planet SET
		"user" = (CASE WHEN m.type = get_colonization_name() THEN (SELECT "user" FROM user_mission WHERE mission = m.id) ELSE planet."user" END),
		metal = planet.metal + m.cargo
		FROM mission m
		WHERE m.target = planet.name AND m.progress = m.time AND return = FALSE AND (m.type = get_colonization_name() OR m.type = get_transport_name());
	
	--COLONIZATION / TRANSPORT-- Update mission.cargo
	UPDATE mission SET cargo = 0 WHERE progress = time AND return = FALSE AND (type = get_colonization_name() OR type = get_transport_name());
	
	UPDATE mission SET progress = 0, return = TRUE WHERE progress = time AND return = FALSE;
	DELETE FROM mission WHERE progress = time AND return;
END $$;

--DROP TRIGGER user_delete_trigger ON "user";
CREATE TRIGGER user_delete_trigger BEFORE DELETE ON "user" FOR EACH ROW EXECUTE PROCEDURE user_delete_procedure();

--DROP TRIGGER user_create_trigger ON "user";
CREATE TRIGGER user_create_trigger AFTER INSERT ON "user" FOR EACH ROW EXECUTE PROCEDURE user_create_procedure();

--DROP TRIGGER building_create_trigger ON building
CREATE TRIGGER building_create_trigger AFTER INSERT ON building FOR EACH ROW EXECUTE PROCEDURE building_create_procedure();

--DROP TRIGGER building_upgrade_trigger ON building
CREATE TRIGGER building_upgrade_trigger AFTER UPDATE ON building FOR EACH ROW EXECUTE PROCEDURE building_upgrade_procedure();

--DROP TRIGGER ship_create_trigger ON ship
CREATE TRIGGER ship_create_trigger AFTER INSERT ON ship FOR EACH ROW EXECUTE PROCEDURE ship_create_procedure();

--DROP TRIGGER mission_delete_trigger ON mission
CREATE TRIGGER mission_delete_trigger BEFORE DELETE ON mission FOR EACH ROW EXECUTE PROCEDURE mission_delete_procedure();
