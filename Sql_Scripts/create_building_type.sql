DELETE FROM building_type;

INSERT INTO building_type VALUES(get_powerstation_name(), 0, 0, 10, 600);
INSERT INTO building_type VALUES(get_metalmine_name(), 250, 50, 30, 300);
INSERT INTO building_type VALUES(get_shipyard_name(), 250, 250, 10, 2000);
INSERT INTO building_type VALUES(get_deutermine_name(), 500, 150, 50, 1500);

/*DELETE FROM building;
INSERT INTO building (type, planet, "level", is_active, construction_progress) VALUES('Elektrownia słoneczna', 'PUGF42728', 3, TRUE, 0);
INSERT INTO building (type, planet, "level", is_active, construction_progress) VALUES('Kopalania metalu', 'PUGF42728', 3, TRUE, 0);
INSERT INTO building (type, planet, "level", is_active, construction_progress) VALUES('Stocznia', 'PUGF42728', 2, TRUE, 0);
INSERT INTO building (type, planet, "level", is_active, construction_progress) VALUES('Ekstraktor deuteru', 'PUGF42728', 1, FALSE, 0);
INSERT INTO building (type, planet, "level", is_active, construction_progress) VALUES('Fabryka robotów', 'PUGF42728', 1, TRUE, 100);*/

--INSERT INTO building (type, planet, "level", is_active, construction_progress) VALUES('Elektrownia słoneczna', 'PQPC74797', 1, TRUE, 0);
--INSERT INTO building (type, planet, "level", is_active, construction_progress) VALUES('Kopalnia metalu', 'PQPC74797', 3, TRUE, 0);
