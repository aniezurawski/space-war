#include <cstdio>
#include <string>
#include <algorithm>

const int GALAXY_COORD_RANGE = 1000000;
const int PLANET_COORD_RANGE = 1000000;
const int GALAXY_NAME_LETTER_LENGTH = 3;
const int PLANET_NAME_LETTER_LENGTH = 3;
const int GALAXY_NAME_DIGIT_LENGTH = 5;
const int PLANET_NAME_DIGIT_LENGTH = 5;
const int ENERGY_POTENTIAL_EXP = 1000;
const int ENERGY_POTENTIAL_VAR = 3;

int galaxy;
int planetExp;
int planetVar;
int seed;

int random(int range)
{
	seed *= 13;
	srand(seed);
	int result = rand();
	seed += result;
	return result % range;
}

int randomVar(int exp, int var)
{
	return random(exp);
}

void randName(std::string& str, int lenLet, int lenDig)
{
	str.resize(lenLet + lenDig);
  
	int range = ('Z' - 'A') + 1;
	for (int i = 0; i < lenLet; i++) {
		int c = random(range) + 'A';
		str[i] = (char)c;
	}
	
	range = ('9' - '0') + 1;
	for (int i = 0; i < lenDig; i++) {
		int c = random(range) + '0';
		str[lenLet + i] = (char)c;
	}
}

void addGalaxy(std::string& name)
{
	randName(name, GALAXY_NAME_LETTER_LENGTH, GALAXY_NAME_DIGIT_LENGTH);
	int coordX = random(GALAXY_COORD_RANGE * 2) - GALAXY_COORD_RANGE;
	int coordY = random(GALAXY_COORD_RANGE * 2) - GALAXY_COORD_RANGE;
	int coordZ = random(GALAXY_COORD_RANGE * 2) - GALAXY_COORD_RANGE;
	
	printf("INSERT INTO galaxy VALUES ('G%s', %d, %d, %d);\n", name.data(), coordX, coordY, coordZ);
}

void addPlanet(const std::string& galaxyName)
{
	std::string planetName;
	randName(planetName, PLANET_NAME_LETTER_LENGTH, PLANET_NAME_DIGIT_LENGTH);
	int coordX = random(PLANET_COORD_RANGE * 2) - PLANET_COORD_RANGE;
	int coordY = random(PLANET_COORD_RANGE * 2) - PLANET_COORD_RANGE;
	int coordZ = random(PLANET_COORD_RANGE * 2) - PLANET_COORD_RANGE;
	int energyPotential = randomVar(ENERGY_POTENTIAL_EXP , ENERGY_POTENTIAL_VAR);
	
	printf("INSERT INTO planet (name, coord_x, coord_y, coord_z, energy_potential, galaxy) VALUES ('P%s', %d, %d, %d, %d, 'G%s');\n",
		   planetName.data(), coordX, coordY, coordZ, energyPotential, galaxyName.data());
}

int main()
{
	scanf("%d%d%d%d", &galaxy, &planetExp, &planetVar, &seed);
	
	for (int i = 0; i < galaxy; i++) {
		std::string name;
		addGalaxy(name);
		
		int planetNumber = randomVar(planetExp, planetVar);
		for (int j = 0; j < planetNumber; j++) {
			addPlanet(name);
		}
	}
	
	return 0;
}
