#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "../Space_War/database.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT

    static const int SERVER_DATA_UPDATE_TIME = 1000;
    
public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void serverUpdateDataTimerSlot();

private:
    DataBase db;
    QTimer serverUpdateDataTimer;

};

#endif // MAINWINDOW_H
