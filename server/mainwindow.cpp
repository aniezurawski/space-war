#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    connect(&serverUpdateDataTimer, SIGNAL(timeout()), this, SLOT(serverUpdateDataTimerSlot()));
    serverUpdateDataTimerSlot();
}

MainWindow::~MainWindow()
{
    
}

void MainWindow::serverUpdateDataTimerSlot()
{
    DataBase::serverUpdateData();
    serverUpdateDataTimer.start(SERVER_DATA_UPDATE_TIME);
}
